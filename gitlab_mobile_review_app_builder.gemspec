# frozen_string_literal: true

require_relative "lib/gitlab_mobile_review_app_builder/version"

Gem::Specification.new do |spec|
  spec.name          = "gitlab_mobile_review_app_builder"
  spec.version       = GitlabMobileReviewAppBuilder::VERSION
  spec.authors       = ["Darby Frey"]
  spec.email         = ["dfrey@gitlab.com"]

  spec.summary       = "Some code to support mobile review apps in GitLab"
  spec.homepage      = "https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/gitlab_mobile_review_app_builder"
  spec.license       = "MIT"
  spec.required_ruby_version = ">= 2.4.0"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/gitlab_mobile_review_app_builder"
  spec.metadata["changelog_uri"] = "https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/gitlab_mobile_review_app_builder/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # Uncomment to register a new dependency of your gem
  # spec.add_dependency "example-gem", "~> 1.0"
  spec.add_dependency "bundler", "~> 2"
  spec.add_dependency "fastlane", "~> 2"
  spec.add_dependency "http", "~> 4"
  spec.add_dependency "thor"

  # For more information and examples about making a new gem, checkout our
  # guide at: https://bundler.io/guides/creating_gem.html
end
